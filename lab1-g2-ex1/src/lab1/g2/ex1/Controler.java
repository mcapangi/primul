
package lab1.g2.ex1;

public class Controler {
    private Lift l; //agregare

    public Controler(Lift l) {
        this.l=l;      
    }
    /**
     * Metoda va deplasa liftul la etajul dorit (urca sau coboara)
     * in functie de etaul curent, dupa care va afisa 'Liftul a ajuns. Se deschid usile...'
     * @param etaj 
     */
    
    void cheamaLift(int etaj)
    {
        while(l.getEtaj()<etaj)
        {
            try
            {
                Thread.sleep(1000);
            }catch(Exception e){};
            l.urca();
        }
        while(l.getEtaj()>etaj)
        {
            try
            {
                Thread.sleep(1000);
            }catch(Exception e){};
            l.coboara();
        }
        System.out.println("Liftul a ajuns. Se deschid usile...");
    }
    
    
}
